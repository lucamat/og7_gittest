import java.util.Scanner;
//@author Luca Bechtel 
public class Primzahlen {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Stoppuhr tester = new Stoppuhr();
		long zahl = 1;
		while (true) {
			System.out.println("Geben Sie die zu überprüfende Zahl ein(0 zum beenden): ");
			zahl = scan.nextLong();
			if (zahl==0) {
				break;
			}
			tester.start();
			if (isPrim(zahl)) {
				tester.stopp();
				if (zahl>0) {
					System.out.println(zahl+" ist eine Primzahl.");
					System.out.println("gebrauchte Zeit(in Nanosekunden): "+tester.getZeit());
				}
				else {
					System.out.println(zahl+" ist eine Primzahl. (negative Zahlen nicht erlaubt)");
					System.out.println("gebrauchte Zeit(in Nanosekunden): "+tester.getZeit());
				}
			}
			else {
				tester.stopp();
				System.out.println(zahl+" ist keine Primzahl.");
				System.out.println("gebrauchte Zeit(in Nanosekunden): "+tester.getZeit());
			}
			tester.reset();
		}
		scan.close();
	}

	public static boolean isPrim(long zahl) {
		if (zahl==0||zahl==1) {
			return false;
		}
		if (zahl<0) {
			zahl*=-1;
		}
		for (int i = 2; i < zahl; i++) {
			if (zahl%i==0) {
				return false;
			}
		}
		return true;
	}
}
