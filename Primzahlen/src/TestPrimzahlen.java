
public class TestPrimzahlen {

	public static void main(String[] args) {
		long[] zahl = {7l,97l,997l,9973l,99991l,999983l,9999991l,99999989l,999999929l,9999999929l,99999999977l};
		Stoppuhr tester = new Stoppuhr();
		for (int i = 0; i < zahl.length; i++) {
			tester.start();
			if (isPrim(zahl[i])) {
				tester.stopp();
					System.out.println(zahl[i]+" ist eine Primzahl.");
					System.out.println("gebrauchte Zeit(in Nanosekunden): "+tester.getZeit());
			}
			else {
				tester.stopp();
				System.out.println(zahl[i]+" ist keine Primzahl.");
				System.out.println("gebrauchte Zeit(in Nanosekunden): "+tester.getZeit());
			}
			tester.reset();
		}
	}
	public static boolean isPrim(long zahl) {
		zahl = Math.abs(zahl);
		if(zahl<2) {
			return false;
		}
		for (int i = 2; i < zahl; i++) {
			if (zahl%i==0) {
				return false;
			}
		}
		return true;
	}
}
