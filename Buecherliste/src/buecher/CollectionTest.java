package buecher;

import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		String titel;
		String autor;
		String isbn;
		Buch gesucht = new Buch("","","");
		List<Buch> buchliste = new LinkedList<Buch>();
		Scanner scan = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = scan.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Geben Sie den Autor des Buches ein: ");
				autor = scan.next();
				System.out.println("Geben Sie Titel des Buches ein: ");
				titel = scan.next();
				System.out.println("Geben Sie die ISBN des Buches ein: ");
				isbn = scan.next();
				Buch b = new Buch(autor,titel,isbn);
				buchliste.add(b);
				System.err.println("Buch hinzugef�gt");
				break;
			case '2':
				System.out.println("Geben Sie die gesuchte Isbn ein: ");
				isbn = scan.next();
				System.out.println(findeBuch(buchliste,isbn));
				System.out.println("An der Stelle " + buchliste.indexOf(findeBuch(buchliste,isbn))+1);
				break;
			case '3':
				System.out.println("Welches Buch wollen Sie l�schen?\n Geben sie dessen Isbn Nummer ein: ");
				gesucht.setIsbn(scan.next());
				if(loescheBuch(buchliste,gesucht)) {
					System.out.println("Buch gel�scht.");
				}
				else {
					System.out.println("Isbn nicht gefunden.");
				}
				break;
			case '4':
				System.out.println("Die gr��te Isbn ist "+ ermitteleGroessteISBN(buchliste));
				break;
			case '5':
				System.out.println(buchliste);
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = scan.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		Buch gesucht = new Buch("","",isbn);
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).equals(gesucht)) {
				return buchliste.get(i);
			}
		}
		gesucht=null;
		return gesucht;
	}

	 public static boolean loescheBuch(List<Buch> buchliste, Buch buch) {
		 for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).equals(buch)) {
				buchliste.remove(i);
				return true;
			}
		}
		 return false;
	 }

	 public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		 String isbn = buchliste.get(0).getIsbn();
		 for (int i = 1; i < buchliste.size(); i++) {
			if(isbn.compareTo(buchliste.get(i).getIsbn())<0) {
				isbn=buchliste.get(i).getIsbn();
			}
		}
		 return isbn;
	 }

}
