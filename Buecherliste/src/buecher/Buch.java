package buecher;

public class Buch implements Comparable<Buch> {
	private String autor;
	private String titel;
	private String isbn;

	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "autor=" + this.autor + ", titel=" + this.titel + ", isbn=" + this.isbn;
	}

	@Override
	public boolean equals(Object b) {
		if (((Buch) b).compareTo(this) == 0) {
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(Buch aBuch) {
		return this.isbn.compareTo(aBuch.isbn);
	}
}
