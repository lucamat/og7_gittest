package addon;

//@author: Sidney Berger und Luca Bechtel

public class Addon {
	
	//Attribute
	private int idNummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestand;
	private int maxbestand;
	
	//Konstruktor
	public Addon() {
		
	}
	
	//parametisierter Konstruktor
	public Addon(int idNummer, String bezeichnung, double verkaufspreis, int bestand, int maxbestand) {
		this.idNummer = idNummer;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.bestand = bestand;
		this.maxbestand = maxbestand;
	}
	
	//Methoden
	public void kaufen() {
		if (this.getBestand() < this.getMaxbestand()) {
			this.setBestand(this.getBestand() + 1);
		}
	}
	
	public void verbrauchen() {
		if (this.getBestand() > 0) {
			this.setBestand(this.getBestand() - 1);
		}
	}
	
	//Zugriffsmethoden
	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxbestand() {
		return maxbestand;
	}

	public void setMaxbestand(int maxbestand) {
		this.maxbestand = maxbestand;
	}
	

}
