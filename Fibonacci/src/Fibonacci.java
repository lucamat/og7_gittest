import java.util.Scanner;

public class Fibonacci {
	
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Zahl n eingeben: ");
		int n= scan.nextInt();	
		System.out.println(fibo(n));
		scan.close();
	}
	
	public static int fibo(int i) {
		if (i<=2) {
		return 1;
		}
		return fibo(i-1) +fibo(i-2);
	}
}
