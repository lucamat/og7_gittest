import java.io.*;
import java.util.ArrayList;

public class FileControl {
	
	public ArrayList<String> lerneintrag = new ArrayList<>();
	
	public void dateiEinlesenUndAusgeben(File file) throws IOException {
   BufferedReader br = new BufferedReader(new FileReader(file));
   String zeile;
   while ((zeile = br.readLine()) != null) {
	   lerneintrag.add(zeile);
   }
   br.close();
	}
}
