package omnom;

public class Haustier {
	private String name;
	private int hunger = 100;
	private int muede = 100;
	private int zufrieden = 100;
	private int gesund = 100;
	
	public Haustier(String name) {
		super();
		this.name = name;
	}

	public Haustier(String name, int hunger, int schlaf, int zufrieden, int gesundheit) {
		super();
		this.name = name;
		this.hunger = hunger;
		this.muede = schlaf;
		this.zufrieden = zufrieden;
		this.gesund = gesundheit;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (0>hunger) {
			hunger=0;
		}
		if (100<hunger) {
			hunger=100;
		}
		this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int schlaf) {
		if (0>schlaf) {
			schlaf=0;
		}
		if (100<schlaf) {
			schlaf=100;
		}
		this.muede = schlaf;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (0>zufrieden) {
			zufrieden=0;
		}
		if (100<zufrieden) {
			zufrieden=100;
		}
		this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesundheit) {
		if (0>gesundheit) {
			gesundheit=0;
		}
		if (100<gesundheit) {
			gesundheit=100;
		}
		this.gesund = gesundheit;
	}
	
	public void fuettern(int futter) {
		this.setHunger(this.getHunger()+futter);
	}
	
	public void schlafen(int schlaf) {
		this.setMuede(this.getMuede()+schlaf);
	}
	
	public void spielen(int spiel) {
		this.setZufrieden(this.getZufrieden()+spiel);
	}
	
	public void heilen() {
		this.setGesund(100);
	}
}
