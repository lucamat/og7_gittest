package keystore;

import java.util.Arrays;

public class KeyStore {
	int stelle=0;
	String[] key;
	
	public KeyStore() {
		key = new String[100];
	}
	public KeyStore(int length) {
		key = new String[length];
	}
	public boolean add(String eintrag) {
		if (stelle<key.length) {
			key[stelle]=eintrag;
			stelle++;
			return true;
		}
		return false;
	}
	public int indexOf(String eintrag) {
		for (int i = 0; i < key.length; i++) {
			if (eintrag==key[i]) {
				return i;
			}
		}
		return -1;
	}
	public boolean remove(int index) {
		if(key[index]!=null) {
			key[index]=null;
			return true;
		}
		return false;
	}
	public boolean remove(String eintrag) {
		int index = indexOf(eintrag);
		if(index>=0) {
			key[index]=null;
			return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return "KeyStore01 [keys=" + Arrays.toString(key) + "]";
	}
	public void clear() {
		for (int i = 0;i < key.length; i++) {
			key[i]=null;
		}
		stelle=0;
	}
	public int size() {
		return key.length;
	}
	public String get(int index) {
		return key[index];
	}

}
