
public class KGV {
	public static String kgv(int a,int b) {
		int zahl1 = a;
		int zahl2= b;
		if (a<1||b<1) {
			return "Fehler";
		}
		while (a!=b) {
			if (a<b) {
				a = a+zahl1;
			} else {
				b = b+zahl2;
			}
		}
		return a+"";
	}
}
