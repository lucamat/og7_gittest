import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test_1 {

	@Test
	void test() {
		assertEquals("Fehler", KGV.kgv(0, 1));
		assertEquals("2", KGV.kgv(2, 2));
		assertEquals("2", KGV.kgv(1, 2));
		assertEquals("9991", KGV.kgv(97, 103));
		assertEquals("Fehler", KGV.kgv(-6, 8));
		assertEquals("Fehler", KGV.kgv(0, 0));
		assertEquals("9", KGV.kgv(3, 9));
		assertEquals("600", KGV.kgv(600, 3));
		assertEquals("2100000000", KGV.kgv(2100000000, 1));
		assertEquals("Fehler", KGV.kgv(-6, -6));
		assertEquals("48", KGV.kgv(16, 3));
	}

}
