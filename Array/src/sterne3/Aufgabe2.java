package sterne3;

import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte die Anzahl der Felder eingeben: ");
		int x = scan.nextInt();
		int[][] zahl = new int[x][x];
		for (int i = 0; i <x; i++) {
			for (int j = 0; j < x; j++) {
				zahl[i][j]=i*j;
				if (j<(x-1)) {
					System.out.print(zahl[i][j]+",");	
				}
				else {
					System.out.println(zahl[i][j]);
				}
			}
		}
		scan.close();

	}

}
