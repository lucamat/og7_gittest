package sterne3;

import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Zahl x eingeben: ");
		int x=scan.nextInt();
		int[] zahl = new int[x];
		for (int i = 0; i < zahl.length-1; i++) {
			zahl[i]=i;
			System.out.print(zahl[i]+", ");
		}
		zahl[zahl.length-1]=zahl.length-1;
		System.out.println(zahl[zahl.length-1]);
		scan.close();

	}

}
