package sterne2;

import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Zahl x eingeben: ");
		int x=scan.nextInt();
		int[] zahl = new int[x];
		for (int i = 0; i < zahl.length; i++) {
			zahl[i]=i;
			System.out.print(zahl[i]+", ");
		}
		scan.close();
	}

}
