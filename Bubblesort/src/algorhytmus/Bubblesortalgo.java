package algorhytmus;

public class Bubblesortalgo {

	public static void main(String[] args) {
		
		int[] unsortiert = {3,19,6,21,17,2,99,76,65,1,48,1002,9,72,34,-9};
		int[] sortiert = bubblesort(unsortiert);
		
		for (int i = 0; i<sortiert.length; i++) {
			if(i==sortiert.length-1) {
				System.out.println(sortiert[i]);
			}
			else {
				System.out.print(sortiert[i] + ", ");
			}
		}

	}

	public static int[] bubblesort(int[] sortiert) {
		int zahl;
		for (int i = 1; i < sortiert.length; i++) {
			for (int j = 0; j < sortiert.length - i; j++) {
				if (sortiert[j] > sortiert[j + 1]) {
					zahl = sortiert[j];
					sortiert[j] = sortiert[j + 1];
					sortiert[j + 1] = zahl;
				}

			}
		}
		return sortiert;
	}
}
