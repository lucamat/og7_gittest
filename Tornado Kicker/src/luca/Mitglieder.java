package luca;

public abstract class Mitglieder {
	private String name;
	private String tel;
	private boolean beitrag;
	
	public Mitglieder() {
		super();
	}

	public Mitglieder(String name, String tel, boolean beitrag) {
		super();
		this.name = name;
		this.tel = tel;
		this.beitrag = beitrag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public boolean isBeitrag() {
		return beitrag;
	}

	public void setBeitrag(boolean beitrag) {
		this.beitrag = beitrag;
	}
	
	
}
