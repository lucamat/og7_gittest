package luca;

public class Trainer extends Mitglieder{
	private char lklasse;
	private int gehalt;
	
	public Trainer(String name, String tel, boolean beitrag) {
		super(name, tel, beitrag);
	}

	public char getLklasse() {
		return lklasse;
	}

	public void setLklasse(char lklasse) {
		this.lklasse = lklasse;
	}

	public int getGehalt() {
		return gehalt;
	}

	public void setGehalt(int gehalt) {
		this.gehalt = gehalt;
	}
	
	
}
