package luca;

public class Schiedsrichter extends Mitglieder{
	private int anzahlSpiele;

	public Schiedsrichter(String name, String tel, boolean beitrag) {
		super(name, tel, beitrag);
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}
}
