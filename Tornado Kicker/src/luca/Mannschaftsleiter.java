package luca;

public class Mannschaftsleiter extends Spieler{
	private String mannschaft;
	private int rabattBeitrag;
	
	public Mannschaftsleiter(String name, String tel, boolean beitrag) {
		super(name, tel, beitrag);
	}
	public String getMannschaft() {
		return mannschaft;
	}
	public void setMannschaft(String mannschaft) {
		this.mannschaft = mannschaft;
	}
	public int getRabattBeitrag() {
		return rabattBeitrag;
	}
	public void setRabattBeitrag(int rabattBeitrag) {
		this.rabattBeitrag = rabattBeitrag;
	}
	
	
}
