package luca;

public class Spieler extends Mitglieder{
	private int trikonr;
	private String pos;
	
	public Spieler(String name, String tel, boolean beitrag) {
		super(name, tel, beitrag);
	}

	public int getTrikonr() {
		return trikonr;
	}

	public void setTrikonr(int trikonr) {
		this.trikonr = trikonr;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}
	
	
}
