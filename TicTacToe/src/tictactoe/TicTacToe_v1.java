package tictactoe;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

@SuppressWarnings("serial")
public class TicTacToe_v1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe_v1 frame = new TicTacToe_v1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicTacToe_v1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		
		for (int i = 0; i < 9; i++) {
			JButton btn = new JButton("");
			btn.setFont(new Font("Tahoma", Font.PLAIN, 80));
			btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					changeBtnText(btn);
				}
			});
			contentPane.add(btn);
		}
		
	}


	private void changeBtnText(JButton b) {
		if (b.getText()=="") {b.setText("X");}
		else if (b.getText()=="X") {b.setText("O");}
		else {b.setText("");}
	}
}